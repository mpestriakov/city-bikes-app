
const initialState = {
  networksDataIsLoading: false,
  stationsDataIsLoading: false,
  hasError: false,
  networks: [],
  stations: [],
  favStations: [],
  currentNetworkId: '',
  currentNetworkName: '',
}

export default function appReducer(state = initialState, action) {


  switch (action.type) {

    case 'NETWORKS_DATA_IS_LOADING':
      return { ...state, networksDataIsLoading: action.networksDataIsLoading };

    case 'STATIONS_DATA_IS_LOADING':
      return { ...state, stationsDataIsLoading: action.stationsDataIsLoading };

    case 'HAS_ERROR':
      return { ...state, hasError: action.hasError };

    case 'SET_NETWORKS':
      return { ...state, networks: action.networks.networks };

    case 'SET_STATIONS':
      return { ...state, stations: action.stations };

    case 'SET_CURRENT_NETWORK_ID':
      return { ...state, currentNetworkId: action.networkId };

    case 'SET_CURRENT_NETWORK_NAME':
      return { ...state, currentNetworkName: action.networkName };

    case 'ADD_FAV_STATION':
      if (state.favStations.find((el) => el.id === action.station.id)) {
        return state;
      } else {
        const newFavStation = { ...action.station, networkId: action.networkId }
        return { ...state, favStations: [...state.favStations, newFavStation] }
      }

    case 'REMOVE_FAV_STATION':
      return {
        ...state,
        favStations: state.favStations.filter(el => el.id !== action.stationId)
      }

    default:
      return state;
  }
}