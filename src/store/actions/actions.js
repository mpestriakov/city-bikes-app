
// ****************** ACTION CREATORS  *******************

export const isLoading = (bool) => {
  return {
    type: 'IS_LOADING',
    isLoading: bool
  }
}

export const networksDataIsLoading = (bool) => {
  return {
    type: 'NETWORKS_DATA_IS_LOADING',
    networksDataIsLoading: bool
  }
}

export const stationsDataIsLoading = (bool) => {
  return {
    type: 'STATIONS_DATA_IS_LOADING',
    stationsDataIsLoading: bool
  }
}

export const hasError = (bool) => {
  return {
    type: 'HAS_ERROR',
    hasError: bool
  }
}

export const setNetworks = (networks) => {
  return {
    type: 'SET_NETWORKS',
    networks
  }
}

export const setStations = (stations) => {
  return {
    type: 'SET_STATIONS',
    stations
  }
}

export const setCurrentNetworkId = (networkId) => {
  return {
    type: 'SET_CURRENT_NETWORK_ID',
    networkId
  }
}

export const setCurrentNetworkName = (networkName) => {
  return {
    type: 'SET_CURRENT_NETWORK_NAME',
    networkName
  }
}

export const addFavStation = (networkId, station) => {
  return {
    type: 'ADD_FAV_STATION',
    networkId,
    station
  }
}

export const removeFavStation = (stationId) => {
  return {
    type: 'REMOVE_FAV_STATION',
    stationId
  }
}

// ******************* THUNKS *********************** 

// WITH ASYNC AWAIT
// export const networksFetch = (url) => (dispatch) => {
//   dispatch(networksDataIsLoading(true));
//   dispatch(stationsDataIsLoading(true));

//   (async () => {
//     try {
//       const response = await fetch(url);
//       const networks = await response.json();
//       dispatch(setNetworks(networks));
//       dispatch(setCurrentNetworkId(networks.networks[0].id));
//       dispatch(setCurrentNetworkName(networks.networks[0].name));
//     } catch {
//       dispatch(hasError(true));
//     } finally {
//       dispatch(networksDataIsLoading(false));
//       dispatch(stationsDataIsLoading(false));
//     }
//   })();
// }

// export const stationsFetch = (url) => (dispatch) => {
//   dispatch(stationsDataIsLoading(true));

//   (async () => {
//     try {
//       const response = await fetch(url);
//       const stations = await response.json();
//       dispatch(setStations(stations.network.stations));
//     } catch {
//       dispatch(hasError(true));
//     } finally {
//       dispatch(stationsDataIsLoading(false));
//     }
//   })();
// }

// WITH THEN

export const networksFetch = (url) => (dispatch) => {
  dispatch(networksDataIsLoading(true));
  dispatch(stationsDataIsLoading(true));

  fetch(url)
    .then(response => response.json())
    .then(networks => {
      dispatch(setNetworks(networks));
      dispatch(setCurrentNetworkId(networks.networks[0].id));
      dispatch(setCurrentNetworkName(networks.networks[0].name));
    })
    .catch(() => {
      dispatch(hasError(true));
    })
    .finally(() => {
      dispatch(networksDataIsLoading(false));
      dispatch(stationsDataIsLoading(false));
    })
}

export const stationsFetch = (url) => (dispatch) => {
  dispatch(stationsDataIsLoading(true));

  fetch(url)
    .then(response => response.json())
    .then(stations => {
      dispatch(setStations(stations.network.stations));
    })
    .catch(() => {
      dispatch(hasError(true));
    })
    .finally(() => {
      dispatch(stationsDataIsLoading(false));
    })
}