import React, { Component } from 'react'
import NetworkList from './NetworkList';
import Stations from './Stations';
import { connect } from 'react-redux';


class NetworksInfo extends Component {
  render() {
    return (
      < div className="row container" >
        {
          this.props.hasError
            ?
            <h5>Sorry, there is a problem with loading data</h5>
            :
            <>
              <div className="col s6">
                <NetworkList />
              </div>
              <div className="col s6">
                <Stations />
              </div>
            </>
        }
      </div >
    )
  }
}

const mapStateToProps = (state) => {
  return {
    hasError: state.hasError
  }
}

export default connect(mapStateToProps)(NetworksInfo);