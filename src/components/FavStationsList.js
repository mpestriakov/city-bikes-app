import React, { Component } from 'react';
import { connect } from 'react-redux';
import LikeBtn from './LikeBtn';

class FavStationsList extends Component {
  render() {
    const { currentNetworkId, favStations } = this.props;

    const favStationsFromCurrentNetwork = favStations.filter(el => el.networkId === currentNetworkId);

    return (
      <ul className="collection">
        {
          favStationsFromCurrentNetwork.length > 0
            ?
            favStationsFromCurrentNetwork.map(item => (
              <li className="collection-item" key={item.id}>
                {item.name}
                <LikeBtn currentStation={item} />
              </li>
            ))
            :
            <li className="collection-item">
              There are no stations in favourites
            </li>
        }
      </ul>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentNetworkId: state.currentNetworkId,
    favStations: state.favStations,
  }
}

export default connect(mapStateToProps)(FavStationsList);
