import React from 'react';

const LoaderSmall = () => {
  return (
    <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
  );
}

export default LoaderSmall;
