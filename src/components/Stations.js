import React, { Component } from 'react';
import AllStationsList from './AllStationsList';
import FavStationsList from './FavStationsList';

export default class Stations extends Component {
  state = {
    showAllStations: true,
    showFavStations: false
  }

  toggleShowAllStations = () => {
    if (!this.state.showAllStations) {
      this.setState({ showAllStations: true, showFavStations: false })
    }
  }

  toggleShowFavStations = () => {
    if (!this.state.showFavStations) {
      this.setState({ showFavStations: true, showAllStations: false })
    }
  }

  render() {
    const { showAllStations, showFavStations } = this.state;
    return (
      <>
        <button
          onClick={this.toggleShowAllStations}
          className="stationsBtn"
          style={showAllStations ? { color: 'black' } : { color: 'rgba(200, 200, 200, .4)' }}
        >
          All stations
        </button>
        <button
          onClick={this.toggleShowFavStations}
          className="stationsBtn"
          style={showFavStations ? { color: 'black' } : { color: 'rgba(200, 200, 200, .4)' }}
        >
          Favourite stations
        </button>
        {showAllStations && <AllStationsList />}

        {showFavStations && <FavStationsList />}
      </>
    )
  }
}
