import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addFavStation, removeFavStation } from '../store/actions/actions';

class LikeBtn extends Component {

  addLike = (currentStation) => {
    this.props.addFavStation(this.props.currentNetworkId, currentStation);
  }

  removeLike = (currentStationId) => {
    this.props.removeFavStation(currentStationId);
  }

  render() {
    const { currentStation, favStations } = this.props;
    return (
      <div className="right">
        {
          favStations.find(el => el.id === currentStation.id)
            ?
            <div onClick={() => this.removeLike(currentStation.id)}>
              <i className="fas fa-heart"></i>
            </div>
            :
            <div onClick={() => this.addLike(currentStation)}>
              <i className="far fa-heart"></i>
            </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    favStations: state.favStations,
    currentNetworkId: state.currentNetworkId,
    networksWithFavStations: state.networksWithFavStations
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addFavStation: (networkId, station) => dispatch(addFavStation(networkId, station)),
    removeFavStation: (stationId) => dispatch(removeFavStation(stationId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LikeBtn);
