import React, { Component } from 'react';
import LoaderSmall from './LoaderSmall';
import { connect } from 'react-redux';
import LikeBtn from './LikeBtn';

class AllStationsList extends Component {

  render() {
    return (
      <>
        {
          this.props.stationsDataIsLoading
            ?
            <div className="center">
              <LoaderSmall />
            </div>
            :
            <ul className="collection">
              {
                this.props.stations.length > 0
                  ?
                  this.props.stations.map(item => (
                    <li className="collection-item" key={item.id}>
                      {item.name}
                      <LikeBtn currentStation={item} />
                    </li>
                  ))
                  :
                  <li className="collection-item">
                    There are no stations in current network
                  </li>
              }
            </ul>
        }
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    stations: state.stations,
    stationsDataIsLoading: state.stationsDataIsLoading
  }
}

export default connect(mapStateToProps)(AllStationsList);