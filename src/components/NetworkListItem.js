import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  setCurrentNetworkId,
  setCurrentNetworkName,
  stationsFetch
} from '../store/actions/actions';

const urlStations = 'https://api.citybik.es/v2/networks/';

class NetworkListItem extends Component {

  handleClick = (networkId, networkName) => {
    if (networkId === this.props.currentNetworkId) {
      return;
    }
    this.props.setCurrentNetworkId(networkId);
    this.props.setCurrentNetworkName(networkName);
    this.props.stationsFetch(`${urlStations}${networkId}`);
  }

  render() {
    const { id, name, currentNetworkId } = this.props;
    return (
      <li
        className={
          currentNetworkId === id ?
            "collection-item active" :
            "collection-item"
        }
        key={id}
        onClick={() => this.handleClick(id, name)}
      >
        {name}
      </li>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentNetworkId: state.currentNetworkId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentNetworkId: (networkId) => dispatch(setCurrentNetworkId(networkId)),
    setCurrentNetworkName: (networkName) => dispatch(setCurrentNetworkName(networkName)),
    stationsFetch: (url) => dispatch(stationsFetch(url))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NetworkListItem);