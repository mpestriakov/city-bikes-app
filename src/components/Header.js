import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoaderSmall from './LoaderSmall';

class Header extends Component {

  render() {
    const {
      favStations,
      currentNetworkId,
      networksDataIsLoading,
      currentNetworkName,
      stationsDataIsLoading,
      stations
    } = this.props;

    const favStationsFromCurrentNetwork = favStations.filter(el => el.networkId === currentNetworkId);

    return (
      <nav>
        <div className="nav-wrapper purple darken-4">
          <div
            className="container"
            style={{
              display: 'flex',
              justifyContent: 'space-between'
            }}>
            <div>
              <span>Current network:</span>
              <span style={{ marginLeft: 10 }}>
                {
                  networksDataIsLoading
                    ?
                    <LoaderSmall />
                    :
                    currentNetworkName
                }
              </span>
            </div>
            <div >
              <span>Stations:</span>
              <span style={{ marginLeft: 10 }}>
                {
                  stationsDataIsLoading
                    ?
                    <LoaderSmall />
                    :
                    stations.length
                }
              </span>
            </div>
            <div>
              <span>Likes:</span>
              <span style={{ marginLeft: 10 }}>
                {
                  stationsDataIsLoading
                    ?
                    <LoaderSmall />
                    :
                    favStationsFromCurrentNetwork.length
                }
              </span>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentNetworkName: state.currentNetworkName,
    currentNetworkId: state.currentNetworkId,
    networksDataIsLoading: state.networksDataIsLoading,
    stationsDataIsLoading: state.stationsDataIsLoading,
    stations: state.stations,
    favStations: state.favStations
  }
}

export default connect(mapStateToProps)(Header);
