import React, { Component } from 'react';
import { connect } from 'react-redux';
import { networksFetch } from '../store/actions/actions';
import LoaderSmall from './LoaderSmall';
import NetworkListItem from './NetworkListItem';

const urlNetworks = 'https://api.citybik.es/v2/networks?fields=id,company,name';

class NetworkList extends Component {

  componentDidMount() {
    this.props.networksFetch(urlNetworks);
  }

  setSelectedNetworId = (networkId) => {
    this.setState({ selectedNetwork: networkId });
  }

  render() {
    const { networksDataIsLoading, networks } = this.props;
    return (
      <>
        <button
          disabled={true}
          className="stationsBtn stationsBtn-disabled"
        >
          Networks
        </button>
        {
          networksDataIsLoading
            ?
            <div className='center'>
              <LoaderSmall />
            </div>
            :
            <ul className="collection">
              {networks.map(item => (
                <NetworkListItem
                  key={item.id}
                  id={item.id}
                  name={item.name}
                />
              ))}
            </ul>
        }
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    networks: state.networks,
    networksDataIsLoading: state.networksDataIsLoading,
    currentNetworkId: state.currentNetworkId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    networksFetch: (url) => dispatch(networksFetch(url)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NetworkList);