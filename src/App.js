import React, { Component } from 'react'
import Header from './components/Header';
import NetworksInfo from './components/NetworksInfo';

export default class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <NetworksInfo />
      </div>
    )
  }
}

